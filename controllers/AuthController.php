<?php
/**
 * Created by PhpStorm.
 * User: Staff 06
 * Date: 09/08/2018
 * Time: 11:04
 */

namespace app\controllers;


use app\models\LoginForm;
use app\models\Notifications;
use app\models\User;
use app\models\Users;
use app\models\UserToken;
use Firebase\JWT\JWT;
use Yii;
use yii\web\Controller;

class AuthController extends Controller
{
    public function actionLogin()
    {
        \Yii::$app->response->format = \Yii::$app->response::FORMAT_JSON;
        $model = new LoginForm();
        if ($model->load(Yii::$app->request->post()) && $model->login()) {
            $user = $model->getUser();
            $token = [
                'exp'=>time()+(60*60*100000),//tempo de expiração do token
                'dados'=>[
                    'nome'=>$user->name,
                    'cidade'=>"Curitiba",
                    'access_token'=> $user->access_token
                ]
            ];

            $jwt = JWT::encode($token,\Yii::$app->params['token_key']);
            return [
                "token"=>$jwt
            ];
        }else{
            Yii::$app->response->statusCode = 401;
            return[
                'error' => 'Credenciais inválidas!'
            ];
        }
    }
    public function  actionRegister()
    {
        $model = new Users();
        $data = Yii::$app->request->bodyParams;
        $data['password']=\Yii::$app->security->generatePasswordHash($data['password']);
        $model->attributes = $data;
        $model->save(false);
    }

}