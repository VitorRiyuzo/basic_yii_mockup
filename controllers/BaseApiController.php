<?php
/**
 * Created by PhpStorm.
 * User: Staff 06
 * Date: 08/08/2018
 * Time: 13:03
 */

namespace app\controllers;


use Firebase\JWT\JWT;
use yii\base\Module;
use yii\behaviors\TimestampBehavior;
use yii\filters\auth\HttpBearerAuth;
use yii\filters\auth\QueryParamAuth;
use yii\rest\ActiveController;

class BaseApiController extends ActiveController
{
    public $serializer = [
        'class'=>'yii\rest\Serializer',
        'collectionEnvelope' => 'data'
    ];

    public function behaviors()
    {
        $behaviors = parent::behaviors();

        $behaviors['authenticator']=[
            'class'=>HttpBearerAuth::className(),
            'except' => [
                'options'
            ]
        ];

        $behaviors['corsFilter'] = [
            'class' => \yii\filters\Cors::className()
        ];

        return $behaviors;
    }
}