<?php

use yii\db\Migration;

/**
 * Class m180807_173552_table_user
 */
class m180807_173552_table_user extends Migration
{
    /**
     * {@inheritdoc}
     */
    public function safeUp()
    {

    }

    /**
     * {@inheritdoc}
     */
    public function safeDown()
    {
        echo "m180807_173552_table_user cannot be reverted.\n";

        return false;
    }


    // Use up()/down() to run migration code without a transaction.
    public function up()
    {
        $this->createTable('users', [
            'id' => $this->primaryKey(),
            'name' => $this->string()->notNull(),
            'email' => $this->string()->notNull(),
            'password' => $this->string()->notNull(),
            'created_at' => $this->dateTime()->notNull()
        ]);
    }

    public function down()
    {
        echo "m180807_173552_table_user cannot be reverted.\n";

        return false;
    }

}
