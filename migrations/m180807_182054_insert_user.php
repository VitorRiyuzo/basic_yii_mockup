<?php

use yii\db\Migration;

/**
 * Class m180807_182054_insert_user
 */
class m180807_182054_insert_user extends Migration
{
    /**
     * {@inheritdoc}
     */
    public function safeUp()
    {

    }

    /**
     * {@inheritdoc}
     */
    public function safeDown()
    {
        echo "m180807_182054_insert_user cannot be reverted.\n";

        return false;
    }


    // Use up()/down() to run migration code without a transaction.
    public function up()
    {
        $this->insert('users',array(
            'email'=>'test1@teste.com',
            'name' =>'User One',
            'password' => md5('test1')
        ));
    }

    public function down()
    {
        echo "m180807_182054_insert_user cannot be reverted.\n";

        return false;
    }

}
