<?php

use yii\db\Migration;

/**
 * Class m180808_130547_table_notifications
 */
class m180808_130547_table_notifications extends Migration
{
    /**
     * {@inheritdoc}
     */
    public function safeUp()
    {

    }

    /**
     * {@inheritdoc}
     */
    public function safeDown()
    {
        echo "m180808_130547_table_notifications cannot be reverted.\n";

        return false;
    }


    // Use up()/down() to run migration code without a transaction.
    public function up()
    {
        $this->createTable('notifications', [
            'id' => $this->primaryKey(),
            'notification' => $this->string()->notNull(),
            'created_at' => $this->dateTime()->notNull()
        ]);
    }

    public function down()
    {
        echo "m180808_130547_table_notifications cannot be reverted.\n";

        return false;
    }

}
