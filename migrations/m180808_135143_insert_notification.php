<?php

use yii\db\Migration;

/**
 * Class m180808_135143_insert_notification
 */
class m180808_135143_insert_notification extends Migration
{
    /**
     * {@inheritdoc}
     */
    public function safeUp()
    {

    }

    /**
     * {@inheritdoc}
     */
    public function safeDown()
    {
        echo "m180808_135143_insert_notification cannot be reverted.\n";

        return false;
    }


    // Use up()/down() to run migration code without a transaction.
    public function up()
    {
        $this->insert('notifications',array(
            'notification'=>'notificação de teste'
        ));
    }

    public function down()
    {
        echo "m180808_135143_insert_notification cannot be reverted.\n";

        return false;
    }

}
