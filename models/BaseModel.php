<?php
/**
 * Created by PhpStorm.
 * User: Staff 06
 * Date: 09/08/2018
 * Time: 15:12
 */

namespace app\models;


use yii\behaviors\TimestampBehavior;

class BaseModel  extends \yii\db\ActiveRecord
{
    public function behaviors()
    {
        return[
            'timestamp' => [
                'class' => TimestampBehavior::className(),
                'createdAtAttribute' => 'created_at',
                'updatedAtAttribute' => null,
                'value' => new \yii\db\Expression('NOW()')
            ]
        ];
    }
}