<?php

namespace app\models;

use Yii;
use yii\behaviors\TimestampBehavior;

/**
 * This is the model class for table "notifications".
 *
 * @property int $id
 * @property string $notification
 * @property string $created_at
 */
class Notifications extends BaseModel
{
    /**
     * {@inheritdoc}
     */
    public static function tableName()
    {
        return 'notifications';
    }


    /**
     * {@inheritdoc}
     */
    public function rules()
    {
        return [
            [['notification'], 'required'],
            [['created_at'], 'safe'],
            [['notification'], 'string', 'max' => 255],
        ];
    }

    /**
     * {@inheritdoc}
     */
    public function attributeLabels()
    {
        return [
            'id' => 'ID',
            'notification' => 'Notification',
            'created_at' => 'Created At',
        ];
    }
}
