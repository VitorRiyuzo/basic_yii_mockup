<?php
namespace app\models;

use Firebase\JWT\ExpiredException;
use Firebase\JWT\JWT;
use yii\db\ActiveRecord;

class User extends ActiveRecord implements \yii\web\IdentityInterface
{
    public static function tableName()
    {
        return 'users';
    }

    /**
     * {@inheritdoc}
     */
    public static function findIdentity($id)
    {
        return static::findOne($id);
    }

    /**
     * {@inheritdoc}
     */
    public static function findIdentityByAccessToken($token, $type = null)
    {
        //echo $token;
        //die;
        //return static::findOne(['access_token'=>$token]);
        try{
            $decoded = JWT::decode($token, \Yii::$app->params['token_key'],['HS256']);
            return static::findOne(['access_token'=>$decoded->dados->access_token]);
        }catch (ExpiredException $ex){
            echo $ex->getMessage();
            return null;
        }
    }

    /**
     * Finds user by username
     *
     * @param string $username
     * @return static|null
     */
    public static function findByUsername($username)
    {
        return static::findOne(['email'=>$username]);
    }

    /**
     * {@inheritdoc}
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * {@inheritdoc}
     */
    public function getAuthKey()
    {
        return $this->authKey;
    }

    /**
     * {@inheritdoc}
     */
    public function validateAuthKey($authKey)
    {
        return $this->authKey === $authKey;
    }

    /**
     * Validates password
     *
     * @param string $password password to validate
     * @return bool if password provided is valid for current user
     */
    public function validatePassword($password)
    {
//        echo \Yii::$app->security->generatePasswordHash('123456');
//        die;
        return \Yii::$app->security->validatePassword($password,$this->getAttribute('password'));
    }
}
